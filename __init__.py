from fabric.api import env, task, local, run

import django  # noqa
import docker  # noqa
import tests  # noqa
import mysql  # noqa
import postgres  # noqa
import pip  # noqa
import node  # noqa
import redis  # noqa


env.use_ssh_config = True
env.DOCKER_IMAGE_NAME = "agesis_image"
env.PROJECT_MODULE_NAME = "agesis"


@task(alias="d")
def dev():

    env.run = local
    env.HOST_PROJECT_DIR = "./"
    env.HOST_LOG_DIRS = []
    env.HOST_UPLOADS = []
    env.PROJECT_DIR = "/var/www/app"
    env.DOCKER_IMAGE_TAG_PREFIX = "dev"
    env.DOCKER_CONTAINER = "agesis-dev"
    env.DOCKER_COMPOSE_FILE = "docker-compose.dev.yml"
    env.RUN_PARAMS = "--settings=agesis.settings.dev"
    env.HOST_SUPERVISOR_FILE = "_configs/supervisor_host_test.conf"
    env.NODE_MODULES_DIR = "templates"
    env.POSTGRES_CONTAINER = "postgres"
    env.POSTGRES_DB = "agesis"
    env.POSTGRES_USER = "postgres"
    env.POSTGRES_LOCALE = None
    env.REDIS_CONTAINER = env.DOCKER_CONTAINER + "-redis"


@task(alias="t")
def test():

    env.hosts = ["tagesis"]
    env.run = run
    env.HOST_PROJECT_DIR = "/var/www/app"
    env.HOST_LOG_DIRS = []
    env.HOST_UPLOADS = []
    env.PROJECT_DIR = "/var/www/app"
    env.DOCKER_IMAGE_TAG_PREFIX = "test"
    env.DOCKER_CONTAINER = "agesis-test"
    env.DOCKER_COMPOSE_FILE = "docker-compose.test.yml"
    env.RUN_PARAMS = "--settings=agesis.settings.test"
    env.HOST_SUPERVISOR_FILE = "_configs/supervisor_host_test.conf"
    env.SUPERVISOR_PROGRAM = "agesis"
    env.POSTGRES_CONTAINER = env.DOCKER_CONTAINER + "-postgres"
    env.POSTGRES_HOST = "postgres"
    env.POSTGRES_DB = "agesis"
    env.POSTGRES_USER = "postgres"
    env.POSTGRES_LOCALE = None
    env.ADDITIONAL_IMAGES = []
    env.REDIS_CONTAINER = env.DOCKER_CONTAINER + "-redis"


@task(alias="p")
def prod():

    env.hosts = ["pagesis"]
    env.run = run
    env.HOST_PROJECT_DIR = "/var/www/app"
    env.HOST_LOG_DIRS = []
    env.HOST_UPLOADS = []
    env.PROJECT_DIR = "/var/www/app"
    env.DOCKER_IMAGE_TAG_PREFIX = "prod"
    env.DOCKER_CONTAINER = "agesis-prod"
    env.DOCKER_COMPOSE_FILE = "docker-compose.prod.yml"
    env.RUN_PARAMS = "--settings=agesis.settings.prod"
    env.HOST_SUPERVISOR_FILE = "_configs/supervisor_host_prod.conf"
    env.SUPERVISOR_PROGRAM = "agesis"
    env.POSTGRES_CONTAINER = env.DOCKER_CONTAINER + "-postgres"
    env.POSTGRES_HOST = "postgres"
    env.POSTGRES_DB = "agesis"
    env.POSTGRES_USER = "postgres"
    env.POSTGRES_LOCALE = None
    env.ADDITIONAL_IMAGES = []
    env.REDIS_CONTAINER = env.DOCKER_CONTAINER + "-redis"
