from fabric.api import task, env, settings

from .docker import run, cp
from .logger import logger


class FabricException(Exception):
    pass


@task
def shell():
    """
    Opens MySQL console (propmpts for password).
    """

    with settings(abort_exception=FabricException):

        try:
            run("mycli -h {} -u {} -p".format(env.MYSQL_HOST, env.MYSQL_USER))
        except:
            logger.error("An attempt to spawn mycli has failed. Fallbacking to native mysql client.")

            run("mysql -h {} -u {} -p".format(env.MYSQL_HOST, env.MYSQL_USER), env.MYSQL_CONTAINER)


@task
def restore(dump, db=None):
    """
    Loads dump to database via "mysql" command.
    """

    if not db:
        db = env.MYSQL_DB

    filename = "/tmp/dump.sql"
    cp(dump, ":" + filename, env.MYSQL_CONTAINER)
    run("mysql -h {} -u {} -p {} < {}".format(env.MYSQL_HOST, env.MYSQL_USER, db, filename), env.MYSQL_CONTAINER)
    run("rm -rf /tmp/dump.sql")


@task
def recreate(db=None):
    """
    Drops and creates database with UTF-8 support."
    """

    if not db:
        db = env.MYSQL_DB

    sql = "DROP DATABASE IF EXISTS {}; CREATE DATABASE {} CHARACTER SET utf8 COLLATE utf8_general_ci;".format(db, db)
    run("mysql -u {} -p -e \"{}\"".format(env.MYSQL_USER, sql), env.MYSQL_CONTAINER)


@task
def dump(db=None, tables=""):
    """
    Dumps database with mysqldump and copies the dump to host machine.
    """

    dump_path = "/tmp/dump.sql"

    if not db:
        db = env.MYSQL_DB

    run("mysqldump -h {} -u {} -p {} {} > {}".format(env.MYSQL_HOST, env.MYSQL_USER, db, tables, dump_path), env.MYSQL_CONTAINER)
    cp(":" + dump_path, dump_path, env.MYSQL_CONTAINER)

    print("\nDump is saved at {}".format(dump_path))
