from fabric.api import task, env

from .docker import run as drun


@task
def install(packages_dir=None):

    node_url = "https://nodejs.org/dist/v10.15.2/node-v10.15.2-linux-x64.tar.gz"

    drun("rm -rf /tmp/node*")
    drun("cd /tmp ; wget -O node.tar.xz '{}'".format(node_url))
    drun("mkdir /tmp/node")
    drun("cd /tmp ; tar -xf node.tar.xz -C node")
    drun("cd /tmp/node/ ; cd $(ls) ; cp -r bin include lib share /usr")

    if packages_dir:
        drun("cd {} ; npm i".format(packages_dir))


@task
def install_grunt():

    drun("npm i -g grunt-cli")


@task
def cmd(cmd):

    drun("cd {} ; npm {}".format(env.NODE_MODULES_DIR, cmd))
