from fabric.api import task, local
from fabric.api import settings
from clint.textui import colored, puts

from .docker import run as drun


"""
@task
def install():

    # Download and unpack chromedriver.
    drun('curl -fsSL https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip -o /tmp/chromedriver.zip')
    drun("unzip /tmp/chromedriver.zip -d /tmp")
    drun("cp /tmp/chromedriver /usr/bin")

    # Add Google Chrome repositoy.
    drun("wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add -")
    drun("echo \"deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main\" >> /etc/apt/sources.list")

    # Install Fluxbox and VNC server.
    drun("apt-get update; apt-get install -y xvfb x11vnc google-chrome-stable")

    # Create password for VNC.
    drun("mkdir -p ~/.vnc && x11vnc -storepasswd secret ~/.vnc/passwd")

    puts(colored.green("\nVNC password is \"secret\"."))


@task
def server():

    to_install = False

    # Check if everything is installed.
    # If not -> install that.
    with settings(warn_only=True):

        result = drun("ls -la /usr/bin/x11vnc")

        if result.failed:
            to_install = True

    if to_install:
        puts(colored.red("\nTest environment is not set up. Going for that ...\n"))
        install()
        puts(colored.green("\nTest environment set up.\n"))

    drun("x11vnc -forever -usepw -create")
"""


@task
def spawn(name="", keep=False):

    drun("./manage.py test {} {}".format("-k" if keep else "", name))
