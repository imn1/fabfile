from fabric import colors


class Logger(object):

    def log(self, message):

        print("")
        print("LOG: {}".format(message))
        print("")

    def success(self, message):

        self.log(colors.green(message))

    def error(self, message):

        self.log(colors.red(message))


logger = Logger()
