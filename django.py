from fabric.api import task, env, local
from clint.textui import colored, puts

from .logger import logger
from docker import run
from pip import install as ins
import glob
import os
import shutil


@task
def cmd(cmd):

    run("./manage.py {}".format(cmd))


@task
def fixtures(data=""):

    run("./manage.py loaddata {}".format(data))


@task
def migrations(app=""):

    run("./manage.py makemigrations {}".format(app))


@task
def migrate(app="", migration="", params=""):

    run("./manage.py migrate {} {} {}".format(app, migration, params))


@task
def shell(interpreter="ipython"):
    """
    Runs IPython (or the given one) shell in the context of project.

    :param str interpreter: Interpreter to be run.
    """

    run("./manage.py shell_plus --{}".format(interpreter))


@task
def reset_migrations(reset=True):
    """
    Resets migration files and applies new initial migrations (fakes them).
    """

    if reset:
        apps = []
        to_be_copied = []

        # Remove all non "_"*.py migrations.
        # Activate _*.py migrations.
        for d in glob.glob(env.PROJECT_MODULE_NAME + "/*/migrations"):

            app_name = d.split("/")[1]
            apps.append(app_name)

            for f in os.listdir(d):

                # Skip __* files.
                if f.startswith("__"):
                    continue

                # Activate saved migrations.
                if f.startswith("_"):
                    to_be_copied.append([os.path.join(d, f), os.path.join(d, f[1:])])

                # Remove migrations.
                else:
                    local("sudo rm -f " + os.path.join(d, f))

            puts(colored.green("Migration files removed from app {}.".format(app_name)))

        # Create new initial migrations.
        migrations(" ".join(apps))

        # Copy saved migrations.
        for source, target in to_be_copied:
            shutil.copyfile(source, target)

    # Prune list of applied migrations - database.
    # cmd("prunemigrations")
    env.run("docker exec -it {} psql -U {} -d {} -c '{}'".format(env.POSTGRES_CONTAINER, env.POSTGRES_USER, env.POSTGRES_DB, "DELETE FROM django_migrations"))

    # Fake all the new migrations.
    migrate("--fake")


@task
def reset_db():
    """
    Recreates database and applies all migrations.
    """

    # Try to stop Celery - access database which prevents
    # to drop it.
    run("supervisorctl stop all")
    cmd("reset_db")
    migrate()

    # Try to re-run Celery.
    run("supervisorctl start all")

    puts(colored.green("\nDatabase has bet reset. You can now load in fixtures. Use \"django.fixtures\" task."))


@task
def test(test_name=None, keep_db=False):
    """
    Runs test(s) from give module or all tests.

    :param str test_name: Test name in dotted notation - i.e. myapp.core.tests.MyTest.
    """

    if not test_name:
        test_name = env.PROJECT_MODULE_NAME

    cmd("test {} -v 3 {}".format(test_name, "--keepdb" if keep_db else ""))


@task
def notebook(install=False):
    """
    Kicks off jupyter with Django context. Pass True if you want to perform installation.
    """

    if install:
        ins("jupyterlab")

    run("NOTEBOOK_ARGUMENTS=\"--ip=0.0.0.0 --allow-root\" ./manage.py shell_plus --notebook")


@task
def messages(lang="cs"):
    """
    Generates translation messages for given|cs language. Seeks py, html, jade files.
    """

    cmd("makemessages -l {} -e py,html,jade --no-location".format(lang))


@task
def log(file="django", count=50):
    """
    Tail-f log "file" with "count" lines from env.PROJECT_DIR directory (enter without extension) or from given path.
    """

    if not file.startswith("/"):
        file = "{}/logs/{}.log".format(env.PROJECT_DIR, file)

    logger.success("Tailing {} file...".format(file))

    run("tail -f -n {} {}".format(count, file))
