from fabric.api import task, env, put, local

from docker import run


@task
def flush():
    """
    Flushes Redis cache.
    """

    run("redis-cli flushall", env.REDIS_CONTAINER)
