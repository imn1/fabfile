from fabric.api import task, env, put, local
from clint.textui import colored, puts

from docker import run, cp, size_hack
from django import migrate

import os
import time


@task
def shell():
    """
    Opens PostgreSQL console - psql.

    Assumes following:
        * Postgres runs in separated container with suffix "postgres".
        * User "postgres" exists and has full access to the server.
    """

    env.run("docker exec -it {}-postgres bash -c '{} psql -U {}'".format(env.DOCKER_CONTAINER, size_hack(), env.POSTGRES_USER))


@task
def dump():
    """
    Creates dump via pg_dumpall and downloads the file to host machine.
    """

    target_file = "/tmp/dump_{}.sql".format(time.strftime("%Y-%m-%d_%H-%M-%S"))

    run("pg_dumpall -U {} -l {} > {}".format(env.POSTGRES_USER, env.POSTGRES_DB, target_file), env.POSTGRES_CONTAINER)
    cp(":{}".format(target_file), "/tmp/dump.sql", env.POSTGRES_CONTAINER, delete=True)

    puts(colored.green("\nDump is saved in /tmp/dump.sql on docker host machine."))


@task
def restore(path, binary_data_only=False):
    """
    Recreates DB and loads given dump (via pg_restore or psql).

    :param str path: Path to the dump file.
    :param bool binary_data_only: Flag if the file is binary and data only.
    """

    file = os.path.basename(path)
    target = os.path.join("/tmp", file)

    # Upload the dump (if needed).
    if env.host is None:
        if target != path:
            local("cp {} {}".format(path, target))
    else:
        put(path, target)

    cp(target, ":" + target, env.POSTGRES_CONTAINER)

    # Stop all services.
    run("supervisorctl stop all")

    # Reset DB.
    run("./manage.py reset_db")

    # Restore the dump
    if binary_data_only:
        migrate()
        run("pg_restore -U {} -a --disable-triggers -d {} {}".format(env.POSTGRES_USER, env.POSTGRES_DB, target), env.POSTGRES_CONTAINER)
    else:
        run("psql -U {} {} < {}".format(env.POSTGRES_USER, env.POSTGRES_DB, target), env.POSTGRES_CONTAINER)

    # Start all services.
    run("supervisorctl start all")


@task
def recreate(db=None):
    """
    Drops and creates database with UTF-8 support."

    :param str db: Database name to be recreated.
    :param bool stop: Flag whether to restrat all services.
    """

    run("supervisorctl stop all")

    if not db:
        db = env.POSTGRES_DB

    sql = "DROP DATABASE IF EXISTS {};".format(db)
    run("psql -U {} -c \"{}\"".format(env.POSTGRES_USER, sql), env.POSTGRES_CONTAINER)
    puts(colored.green("Database has been dropped."))

    if env.POSTGRES_LOCALE:
        sql = 'CREATE DATABASE {} ENCODING UTF8 LC_COLLATE=\\"{locale}.utf8\\" LC_CTYPE=\\"{locale}.utf8\\" TEMPLATE template0;'.format(db, locale=env.POSTGRES_LOCALE)
    else:
        sql = "CREATE DATABASE {};".format(db)

    run("psql -U {} -c \"{}\"".format(env.POSTGRES_USER, sql), env.POSTGRES_CONTAINER)
    run("supervisorctl start all")
    puts(colored.green("Database has been created."))
