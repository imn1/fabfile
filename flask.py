from fabric.api import task, env

# from .logger import logger
from docker import run


@task
def migrate():
    """
    Migrates PeeWee database models.
    """

    run("python3 migrate.py")
