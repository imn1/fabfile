from fabric.api import task, local, env, sudo, settings
from fabric.context_managers import cd
from fabric.operations import put
from fabric.contrib.files import exists
from clint.textui import colored, puts

# from .logger import logger

from distutils.util import strtobool
import os
import re


def size_hack():
    """
    Sets terminal size to host terminal size.

    :return: The hack line.
    :rtype: str
    """

    if hasattr(env, "COLUMNS") and hasattr(env, "LINES"):
        return "stty cols {} rows {} &&".format(env.COLUMNS, env.LINES)

    return ""


@task
def install():
    """
    Instals Docker to the machine. Assumes user has sudo priviledges.
    """
    packages = [
        "apt-transport-https",
        "ca-certificates",
        "curl",
        "gnupg2",
        "software-properties-common",
        "supervisor",
    ]
    sudo("apt-get update")
    sudo("apt-get install -y {}".format(" ".join(packages)))
    sudo('curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -')
    sudo('add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"')
    sudo("apt-get update; apt-get install -y docker-ce docker-compose")


@task
def run(cmd, container=None):
    """
    Runs shell command inside project directory.
    """

    cd = False

    if not container:
        container = env.DOCKER_CONTAINER
        cd = True

    cmd = "cd {}; {} {}".format(env.PROJECT_DIR if cd else "", cmd, env.RUN_PARAMS if "manage.py" in cmd else "")

    return env.run("docker exec -it {} bash -c '{}'".format(container, cmd))


@task
def restart(service="app"):
    """
    Restarts supervisor service. Defaults to "app".
    """

    run("supervisorctl restart {}".format(service))


@task
def shell(container_postfix=None):
    """
    Opens interactive shell (bash) inside docker container.
    """

    if container_postfix:
        env.run("docker exec -it {} bash -c '{} bash'".format("{}-{}".format(env.DOCKER_CONTAINER, container_postfix), size_hack()))

    else:
        env.run("docker exec -it {} bash -c '{} bash'".format(env.DOCKER_CONTAINER, size_hack()))


@task
def log(service="app", container=None, count=50):
    """
    Tail-f log "file" with "count" lines from /var/log/supervisor directory (enter without extension) or from given path.
    """

    if not service.startswith("/"):
        service = "/var/log/supervisor/{}.log".format(service)

    run("tail -f -n {} {}".format(count, service), container)


@task
def up(detach=False):
    """
    Kicks off the app - and if param "detach" is true it detaches.
    """

    with cd(env.HOST_PROJECT_DIR):
        env.run("docker-compose -f {} -p {} up {}".format(
            env.DOCKER_COMPOSE_FILE,
            env.DOCKER_CONTAINER,
            "-d" if bool(strtobool(str(detach))) else ""
        ))


@task
def down(remove=False, volumes=False, container=None):
    """
    Stops running container, removes them and if "remove" param is true, also removes images.
    """

    if not container:
        container = env.DOCKER_CONTAINER

    with cd(env.HOST_PROJECT_DIR):
        env.run("docker-compose -f {} -p {} down {} {}".format(
            env.DOCKER_COMPOSE_FILE,
            container,
            "-v" if strtobool(str(volumes)) else "",
            "--rmi=all" if strtobool(str(remove)) else "")
        )

        # Logging.
        puts(colored.green("Container {} has been put down on the server.".format(container)))

        if volumes:
            puts(colored.green("Volumes too."))

        if remove:
            puts(colored.green("And all images too."))


@task
def stop():
    """
    Stops running containers.
    """

    with cd(env.HOST_PROJECT_DIR):
        env.run("docker-compose -f {} -p {} stop".format(env.DOCKER_COMPOSE_FILE, env.DOCKER_CONTAINER))


@task
def build(image="app"):
    """
    Builds an image - defaults to "app".
    """

    local("docker-compose -f {} build --no-cache {}".format(env.DOCKER_COMPOSE_FILE, image))
    puts(colored.green("Image {} has been build.".format(image)))


@task
def cp(source, target, container=None, delete=False):
    """
    Copies specific file/directory to/from the container.
    The path which starts with ":" is considered as container path.
    """

    if not container:
        container = env.DOCKER_CONTAINER

    # Add container name to one of source/target.
    if source.startswith(":"):

        # For remote environment use ssh to delete original file before copy operation.
        if delete and env.host is not None and exists(target):
            try:
                puts("Need to delete existing file {}. Please confirm sudo operation.".format(target))
                sudo("rm {}".format(target))
                puts(colored.green("Existing file deleted."))

            except:
                puts("File {} does not exist. Continuing with copy operation.".format(target))

        source = container + source

    elif target.startswith(":"):
        target = container + target

    with cd(env.HOST_PROJECT_DIR):
        env.run("docker cp {} {}".format(source, target))


@task
def cpapp():
    """
    Copies app directory content to the container - app update instead of rebuilding whole container.
    """

    cp(env.HOST_PROJECT_DIR, env.PROJECT_DIR)


@task
def hotfix(path, target=None, restart=True):
    """
    Copies a file to the container. If no target is specified copies the file to the "source" file path.
    """

    file = os.path.basename(path)
    tmp_path = os.path.join("/tmp", file)
    put(path, "/tmp")
    cp(tmp_path, target if target else ":" + os.path.join(env.PROJECT_DIR, path))

    if strtobool(str(restart)):
        sudo("supervisorctl restart {}".format(env.SUPERVISOR_PROGRAM))


@task
def release(tag="latest", make_build=True, upload=True, load_only=False, remove_images=True):
    """
    Builds, exports, packs, transfers to server, unpacks and loads the image. Recreates the containers (down + up).
    """

    def transfer(image, fake=False):
        """
        Transfers image to the server and returns image location
        on the server.

        :param str image: Image name (including tag) to be transfered.
        :param bool fake: Fakes image upload - returns only the image path.
        :return: Image location on the server.
        :rtype: str
        """

        # Create image name from image name.
        image_name = re.sub(r"[^\w]", "_", image)
        image_path = "/tmp/{}.tar".format(image_name)

        if not fake:
            # Export.
            local("docker save {} -o {}".format(image, image_path))
            local("pbzip2 -f {}".format(image_path))

            # Upload.
            for h in env.hosts:
                local("rsync -vhP {}.bz2 {}:/tmp".format(image_path, h))

            puts(colored.green("Image file {} has been transfered onto the server.".format(image_path)))

            # If older unpacked image exists remove it.
            if exists(image_path):
                sudo("rm -rf " + image_path)

            env.run("bzip2 -df {}.bz2".format(image_path))
            puts(colored.green("Image file {} has been unpacked on the server.".format(image_path)))

        return image_path

    # If building for localhost remove existing
    # containers and images first.
    if env.host is None:
        sudo("supervisorctl stop {}".format(env.SUPERVISOR_PROGRAM))
        down(strtobool(str(remove_images)))

    if strtobool(str(make_build)):
        build()

    # If not on localhost transfer the main image to the host.
    remote_image_paths = [transfer(
        "{}:{}-{}".format(env.DOCKER_IMAGE_NAME, env.DOCKER_IMAGE_TAG_PREFIX, tag),
        fake=not strtobool(str(upload))
    )]

    # Also check for additional images.
    if env.hosts is not None and strtobool(str(upload)):

        # Check for additional images required to be also transfered to the server.
        for i in env.ADDITIONAL_IMAGES:
            remote_image_paths.append(transfer(i))

    # Make directories for logs and project dir.
    sudo("mkdir -p {}".format(env.HOST_PROJECT_DIR))

    for d in env.HOST_LOG_DIRS:
        sudo("mkdir -p {}".format(d))

    # Stop all the containers.
    sudo("supervisorctl stop {}".format(env.SUPERVISOR_PROGRAM))

    # If not on localhost remove existing
    # container and images and load the new image(s).
    if env.hosts is not None:

        with settings(warn_only=True):
            down(strtobool(str(remove_images)))

        for p in remote_image_paths:
            env.run("docker load -i {}".format(p))
            puts(colored.green("Image from file {} has been loaded onto the server.".format(p)))

    # If we needed to only transfer the image to the server
    # and do not perform any other actions let's exit here.
    if load_only:
        return

    # Copy fresh docker-compose file.
    put(env.DOCKER_COMPOSE_FILE, env.HOST_PROJECT_DIR, use_sudo=True)
    puts(colored.green("Fresh compose file has been transfered onto the server."))

    # Copy fresh host supervisor conf.
    put(env.HOST_SUPERVISOR_FILE, "/etc/supervisor/conf.d/", use_sudo=True)
    sudo("supervisorctl update")
    puts(colored.green("Fresh supervisor file has been transfered and loaded onto the server."))

    # Copy additional specific files.
    if env.HOST_UPLOADS:
        for u in env.HOST_UPLOADS:

            # 3 items - source, target, new remote dir.
            # if 3 == len(u):
            if 3 == len(u) and not exists(u[2], use_sudo=True):
                sudo("mkdir -m o+w " + u[2])
                puts(colored.green("Directory {} has been created on the server.".format(u[2])))

            # Perform upload.
            put(u[0], u[1], use_sudo=True)
            puts(colored.green("File/directory {} has been transfered onto the server to location {}.".format(u[0], u[1])))

    # Start the app.
    sudo("supervisorctl start {}".format(env.SUPERVISOR_PROGRAM))
    puts(colored.green("Supervisor has been restarted."))
