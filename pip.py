from fabric.api import task

from docker import run


@task
def install(package, version=None):
    """
    Installs package via PIP.

    :param str package: Package to be installed.
    :param str version: Version of the package.
    """

    if version:
        package = "{}=={}".format(package, version)

    run("pip3 install {}".format(package))


@task
def uninstall(package):
    """
    Uninstalls package from system via PIP.

    :param str package: Package to be installed.
    """

    run("pip3 uninstall {}".format(package))


@task
def requirements():
    """
    Installs all requirements from "requirements" file.
    """

    run("pip3 install -r requirements")


@task
def freeze():
    """
    Freezes currently installed packages.
    """

    run("pip3 freeze > requirements")


@task
def github(package, url):
    """
    Installs package from Github.
    Uses -e flag which preserves URL upon package and "freeze" command works right.

    :param str package: Package to be installed
    :param str url: Github repo URL.
    """

    run("pip3 install -e git+{}#egg={}".format(url, package))
